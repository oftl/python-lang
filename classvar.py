class Foo():
    # a classvar
    cv = 'Foo.cv original'

    # access above classvar
    @classmethod
    def get_cv(cls): return cls.cv

print ('instantiate f1:Foo and set instance variable cv')
f1 = Foo()
f1.cv = 'f1.cv altered'

print ('f1.get_cv(): %s' % f1.get_cv())

print ('alter class variable Foo.cv')
Foo.cv = 'Foo.cv altered'

print ('instantiate f2:Foo and set instance variable cv')
f2 = Foo()
f2.cv = 'f2.cv altered'

print ('f1.cv: %s' % f1.cv)
print ('f1.get_cv(): %s' % f1.get_cv())

print ('f2.cv: %s' % f2.cv)
print ('f2.get_cv(): %s' % f2.get_cv())
